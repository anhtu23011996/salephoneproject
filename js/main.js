let arr = [];
let cart = [];
//////////////////////////////////////////////
const saveDataToLocal = function (cart) {
  localStorage.setItem("cartList", JSON.stringify(cart));
};

const getDataFromLocal = function () {
  const tempData = JSON.parse(localStorage.getItem("cartList"));
  if (!tempData) {
    cart = [];
    return;
  }
  cart = tempData;
};
////////////////////////////////////////////////
const render = function (list = arr) {
  var htmlContent = "";
  for (let i of list) {
    const coHinh = CheckImg(i.image);

    if (coHinh) {
      htmlContent += `<div class="col-lg-4 col-md-6 mb-4 ">
                <div class="card h-100 ">
                  <div><a href="#"
                    ><img
                      class="card-img-top"
                      src="${i.image}"
                      alt=""
                      height ="250px"
                  /></a></div>`;
    } else {
      htmlContent += `<div class="col-lg-4 col-md-6 mb-4 ">
                <div class="card h-100 ">
                  <div><a href="#"
                    ><img
                      class="card-img-top"
                      src="https://via.placeholder.com/700x700?text=ĐÉO CÓ HÌNH ĐÂU MÀ NHÌN"
                      alt=""
                  /></a></div>
                  `;
    }
    htmlContent += `
                  <div class="card-body product-card-body">
                    <h4 class="card-title">
                      <a href="#" class="productName">${i.name}</a>
                    </h4>
                    <h5> Price: ${i.price} $</h5>
                    <p class="card-text card-description">
                      ${i.description}
                    </p>
                  </div>`;
    var starFull = "&#9733".repeat(+i.rating);
    var starEmpty = "&#9734".repeat(5 - Math.floor(+i.rating));

    htmlContent += `<div class="card-footer">
                    <small class="text-muted"
                      >${starFull + starEmpty} </small
                    >
                    <span>In stock: ${i.inventory}</span>
                    <span class="buy-button" onClick="putProductToBasket(${
                      i.id
                    })">Mua hàng</span>
                  </div>
                </div>
              </div>`;
  }
  document.getElementById("renderProduct").innerHTML = htmlContent;
};

/* RENDER CATEGORIES*/
const filterType = (list = arr) => {
  console.log(list);
  return list.reduce((accumulator, item) => {
    return !item.type
      ? accumulator.concat("other")
      : accumulator.concat(item.type.toLowerCase());
  }, []);
};

const renderCategory = () => {
  let htmlCategory = "";
  let typeList = filterType(arr);
  const setType = new Set(typeList);
  typeList = [...setType];
  console.log(typeList);
  for (prop of typeList) {
    htmlCategory += `<a href="#" class="list-group-item">${prop.toUpperCase()}</a>`;
  }
  console.log(htmlCategory);
  document.getElementById("category-side-bar").innerHTML = htmlCategory;
};

const fetchApiAndRender = function () {
  if (document.getElementById("basket-count")) {
    getDataFromLocal();
    document.getElementById("basket-count").innerHTML = cart.length;
  }
  axios({
    method: "GET",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
  })
    .then(function (res) {
      arr = res.data;
      render(arr);
      renderCategory(); //de tam
    })
    .catch(function (err) {
      console.log(err);
    });
};

/*   MAIN   */
fetchApiAndRender();

const sortByName = function (option) {
  switch (option) {
    case "1":
      arr.sort(function (a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
        if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
        return 0;
      });
      break;
    case "2":
      arr.sort(function (a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) return 1;
        if (a.name.toLowerCase() > b.name.toLowerCase()) return -1;
        return 0;
      });
      break;
  }
  render(arr);
};

const sortByType = function (option) {
  switch (option) {
    case "1":
      const iphones = arr.filter(
        (iphone) => iphone.type.toLowerCase() === "iphone"
      );

      render(iphones);
      break;
    case "2":
      const samsungs = arr.filter(
        (samsung) => samsung.type.toLowerCase() === "samsung"
      );
      render(samsungs);
  }
};

const putProductToBasket = function (id) {
  const itemPicked = arr.find(function (item) {
    return +id === +item.id;
  });
  if (itemPicked) {
    if (!checkQuantity(itemPicked.inventory)) {
      alert("Sản phẩm hết hàng");
      return;
    }

    //Check sp ton tai trong mang gio hang chua
    const indexOfItemInCart = cart.findIndex(function (item) {
      return +item.product.id === +id;
    });
    console.log(itemPicked);
    if (indexOfItemInCart === -1) {
      //tao doi tuong roi moi push
      const newAddInItem = {
        id: id,
        price: itemPicked.price,
        name: itemPicked.name,
        image: itemPicked.image,
      };

      const instanceOfDienThoai = new CartItem(newAddInItem, 1);
      cart.push(instanceOfDienThoai);
    } else {
      cart[indexOfItemInCart].quantity += 1;
    }
  }

  saveDataToLocal(cart);
  document.getElementById("basket-count").innerHTML = cart.length;
  itemPicked.inventory = (+itemPicked.inventory - 1).toString();

  axios({
    method: "PUT",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    data: itemPicked,
  })
    .then(function (res) {
      fetchApiAndRender();
    })
    .catch(function (err) {
      console.log(err);
    });
};

/* Link trang checkOut*/
const goToCheckOut = function () {
  window.location.assign("cart.html");
};

getDataFromLocal();
console.log("Gia Tri CART ARR", cart);

const renderCart = function (list = cart) {
  console.log(list);
  var htmlCardContent = "";
  for (let i of list) {
    console.log(i.product.image);
    htmlCardContent += `
      <tr>
                        <th scope="row" class="border-0">
                          <div class="p-2">
                            <img
                              src="${i.product.image}"
                              alt=""
                              width="70"
                              class="img-fluid rounded shadow-sm"
                            />
                            <div class="ml-3 d-inline-block align-middle">
                              <h5 class="mb-0">
                                <a
                                  href="#"
                                  class="text-dark d-inline-block align-middle"
                                  >${i.product.name} ($${i.product.price})</a
                                >
                              </h5>
                            </div>
                          </div>
                        </th>
                        <td class="border-0 align-middle">
                          <strong>$${i.product.price * i.quantity}</strong>
                        </td>
                        <td class="border-0 align-middle">
                          <button onclick="changeQuantity(${
                            i.product.id
                          }, 'minus')">-</button><strong>${
      i.quantity
    }</strong> <button onclick="changeQuantity(${
      i.product.id
    },'plus')">+</button>
                        </td>

                        <td class="border-0 align-middle">
                          <button class="btn btn-danger" onclick="deleteItemFromCart(${
                            i.product.id
                          })" class="text-dark"
                            ><i class="fa fa-trash"></i
                          ></button>
                        </td>
                      </tr>
      `;
    console.log(htmlCardContent);
  }
  document.getElementById("renderCartList").innerHTML = htmlCardContent;
  calcTotalPrice();
};

const changeQuantity = function (id, operator) {
  const listPhoneCart = JSON.parse(localStorage.getItem("cartList"));
  for (let item of listPhoneCart) {
    if (item.product.id === id) {
      switch (operator) {
        case "plus":
          item.quantity += 1;
          break;
        case "minus":
          if (item.quantity === 1) {
            break;
          } else {
            item.quantity -= 1;
          }
          break;
      }
    }
  }
  saveDataToLocal(listPhoneCart);
  renderCart(listPhoneCart);
};

const deleteItemFromCart = function (id) {
  const listPhoneInCart = JSON.parse(localStorage.getItem("cartList"));
  const indexItemToDelete = listPhoneInCart.findIndex(function (item) {
    return +item.product.id === +id;
  });
  if (indexItemToDelete !== -1) {
    listPhoneInCart.splice(indexItemToDelete, 1);
  }
  saveDataToLocal(listPhoneInCart);
  renderCart(listPhoneInCart);
};

calcTotalPrice = function () {
  const listPhoneInCart = JSON.parse(localStorage.getItem("cartList"));
  let totalPrice = 0;
  for (let item of listPhoneInCart) {
    totalPrice += item.product.price * item.quantity;
  }
  document.getElementById("totalPrice").innerHTML = `$${totalPrice}`;
};

proceedToCheck = function () {
  localStorage.setItem("cartList", "[]");
  renderCart(JSON.parse(localStorage.getItem("cartList")));
  alert("Thanh toán thành công");
};
