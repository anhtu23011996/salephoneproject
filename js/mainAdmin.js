const apiHandler = new ApiServer();
let productArr = [];

/*Get data put into productArr*/

const transferToEditPage = function (id) {
  console.log("id san pham", id);
  window.location.assign("./edit-product.html?id=" + id);
};

const renderMaintable = function (list = productArr) {
  let htmlContent = "";
  let stt = 1;
  for (let i of list) {
    htmlContent += `<tr>
          <th scope="row">${stt}</th>
          <td class="tm-product-name">${i.name}</td>
          <td>${+i.inventory}</td>
          <td>${i.price}</td>         
          <td>
            <a  class="tm-product-delete-link" ">
              <i class="far fa-trash-alt tm-product-delete-icon" onclick="deleteProduct(${
                i.id
              })" ></i>
            </a>
          </td>
          <td>
            <a class="tm-product-edit-link">
              <i class="far fa-edit tm-product-edit-icon" onclick="transferToEditPage(${
                i.id
              })"></i>
            </a>
          </td>
        </tr>`;
    stt++;
  }
  document.getElementById("productMainTableBody").innerHTML = htmlContent;
};

/* RENDER CATEGORIES*/
const filterType = (list = productArr) => {
  console.log(list);
  return list.reduce((accumulator, item) => {
    return !item.type
      ? accumulator.concat("other")
      : accumulator.concat(item.type.toLowerCase());
  }, []);
};

const renderCategory = () => {
  let htmlCategory = "";
  let typeList = filterType(productArr);
  const setType = new Set(typeList);
  typeList = [...setType];
  console.log(typeList);
  for (prop of typeList) {
    htmlCategory += `<tr>
    <td class="tm-product-name">${prop.toUpperCase()}</td>
    <td class="text-center">
    </td>
  </tr>`;
  }
  console.log(htmlCategory);
  document.getElementById("category-side-bar").innerHTML = htmlCategory;
};

const pullAndRenderFromApi = function () {
  apiHandler
    .callGetMethod()
    .then(function (res) {
      productArr = res.data;
      renderMaintable();
      renderCategory();
    })
    .catch(function (err) {
      console.log(err);
    });
};

const deleteProduct = function (id) {
  console.log("ID can xoa", id);
  axios({
    method: "DELETE",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
  })
    .then(function (res) {
      console.log("Xoa thanh cong");
      pullAndRenderFromApi();
    })
    .catch(function (err) {
      console.log(err);
    });
};
/********* MAIN ************* */
pullAndRenderFromApi();

/**** ADD PRODUCT PAGE */
const getInput = function (elementId) {
  return document.getElementById(elementId).value;
};

const postNewProduct = function () {
  //CHECk form input du chua
  const valid = checkInputForm();

  //Tao doi tuong moi
  if (!valid) return;

  const newProduct = new ProductClass(
    "1",
    getInput("name"),
    getInput("image"),
    getInput("description"),
    getInput("price"),
    getInput("stock"),
    getInput("rating"),
    getInput("category")
  );
  //   //dung axios post len
  axios({
    method: "POST",
    url: " https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    data: newProduct,
  })
    .then(function (res) {
      console.log(res);
      document.getElementById("addProductForm").reset();
      alert("Success!");
    })
    .catch(function (err) {
      console.log(err);
    });

  //   //dung axios get ve render ra
};

/*EDIT PRODUCT PAGE*/

let idOfEditProduct = "";
const renderEditPage = function () {
  const currentUrl = window.location.href;
  const idOfProduct = currentUrl.split("=")[1];
  axios({
    method: "GET",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
  })
    .then(function (res) {
      productArr = res.data;
      const productEditing = productArr.find(function (item) {
        return +item.id === +idOfProduct;
      });
      document.getElementById("name").value = productEditing.name;
      document.getElementById("description").value = productEditing.description;
      document.getElementById("category").value = productEditing.type;
      document.getElementById("price").value = productEditing.price;
      document.getElementById("stock").value = productEditing.inventory;
      document.getElementById("image").value = productEditing.image;
      document.getElementById("showImage").src = productEditing.image;
      document.getElementById("rating").value = productEditing.rating;
      idOfEditProduct = productEditing.id;
    })
    .catch(function (err) {
      console.log(err);
    });
};
renderEditPage();

const saveEditedDetails = function (id) {
  console.log("ID se thay doi", id);
  //CHECk form input du chua
  console.log(document.getElementById("stock").value);
  console.log(document.getElementById("image").value);
  const valid = checkInputForm();
  //Tao doi tuong moi
  if (!valid) return;
  const modifiedProduct = new ProductClass(
    "1",
    getInput("name"),
    getInput("image"),
    getInput("description"),
    getInput("price"),
    getInput("stock"),
    getInput("rating"),
    getInput("category")
  );
  axios({
    method: "PUT",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    data: modifiedProduct,
  })
    .then(function (res) {
      console.log(res);
      document.getElementById("addProductForm").reset();
      alert("Successful!");
    })
    .catch(function (err) {
      console.log(err);
    });
};
