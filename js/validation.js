const annoucement = ["This field cannot be empty"];

const CheckImg = function (img) {
  return img.includes("http");
};

const checkQuantity = function (inventory) {
  if (+inventory > 0) return true;
  return false;
};

const checkEmptyInput = function (id, spid) {
  if (!document.getElementById(id).value) {
    document.getElementById(spid).innerHTML = annoucement[0];
    return false;
  } else {
    document.getElementById(spid).innerHTML = "";
    return true;
  }
};

const checkInputForm = function () {
  let valid = true;
  valid &= checkEmptyInput("name", "spname");
  valid &= checkEmptyInput("description", "spdescription");
  valid &= checkEmptyInput("category", "spcategory");
  valid &= checkEmptyInput("price", "spprice");
  valid &= checkEmptyInput("rating", "sprating");
  valid &= checkEmptyInput("stock", "spstock");
  valid &= checkEmptyInput("image", "spimage");
  return valid;
};
